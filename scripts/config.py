from PyTango import *
import sys
import time

try :

	d = DeviceProxy("test/ica/smr")

	d.write_attribute("numRegions", 3)
	d.write_attribute("mode", ("Swept","Fixed","Swept"))
	d.write_attribute("passEnergy", (200, 100,200))
	d.write_attribute("lensMode", ("Transmission","Ang25","Transmission"))
	d.write_attribute("stepTime", (100, 150, 200))
	d.write_attribute("energyStep", (0.25, 0.5, 0.5))
	d.write_attribute("fixEnergy", (64.0, 30.0, 50.0))
	d.write_attribute("lowEnergy", (5.0, 20.0, 10.0))
	d.write_attribute("highEnergy",(100.0, 30.0, 60.0))
	d.write_attribute("ADCMode", 1)
	d.write_attribute("detectorSlices", 260)
	d.write_attribute("detectorLastYChannel", 811)
	d.write_attribute("detectorFirstYChannel", 247)
	d.write_attribute("detectorLastXChannel", 1291)
	d.write_attribute("detectorFirstXChannel", 158)
	d.write_attribute("passMode", "High Pass (H)")
	d.write_attribute("excitationEnergy", 69.193)

except DevFailed:
	exctype , value = sys.exc_info()[:2]
	print "Failed with exception !", exctype
	for err in value :
		print "---ERROR ELEMENT-------"
		print "reason:" , err['reason']
		print "description:" , err["desc"]
		print "origin:" , err["origin"]
		print "severity:" , err["severity"]
