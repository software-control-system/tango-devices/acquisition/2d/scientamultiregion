// ============================================================================
//
// = CONTEXT
//		TANGO Project - ScientaMultiRegion
//
// = File
//		MultiRegionAcqTask.cpp
//
// = AUTHOR
//		N.Leclercq - SOLEIL & JC.Pret - APSIDE
//
// ============================================================================
#include <MultiRegionAcqTask.h>
#include <yat/time/Timer.h>

// ============================================================================
// SOME USER MESSAGES AND CONSTS
// ============================================================================
#define kSTART_PERIODIC_MSG (yat::FIRST_USER_MSG + 1000)
#define kSTOP_PERIODIC_MSG  (yat::FIRST_USER_MSG + 1001)
#define kSTART_ACQUISITION_MSG    (yat::FIRST_USER_MSG + 1002)
#define kSTOP_ACQUISITION_MSG    (yat::FIRST_USER_MSG + 1003)
#define kVALIDATE_CONFIG_MSG    (yat::FIRST_USER_MSG + 1004)
#define kRESET_CONFIG_MSG    (yat::FIRST_USER_MSG + 1005)

#define SACQ_STANDBY_STATE Tango::STANDBY
#define SACQ_RUNNING_STATE Tango::RUNNING
#define SACQ_FAULT_STATE Tango::FAULT

namespace ScientaMultiRegion_ns
{

const std::string k_spect_attr_names[] = { "channelScaleR", "sliceScaleR", "sumDataR" };
const std::string k_image_attr_name = "dataR";

// Constructors

MultiRegionAcqTask::MultiRegionAcqTask(Tango::DeviceImpl *host_device)
 : yat4tango::DeviceTask(host_device), m_dam( host_device)
{
}

MultiRegionAcqTask::MultiRegionAcqTask(Tango::DeviceImpl *host_device, long max_reg_num, const std::string &target_dev )
 : yat4tango::DeviceTask(host_device), m_dam( host_device)
{
  m_max_region_num = max_reg_num;
  m_host_device = host_device;

  m_data_has_changed = false;

  m_target_proxy =  NULL;
  std::string str = target_dev;
  try
  {
    m_target_proxy = new Tango::DeviceProxy( str );
  }
  catch( Tango::DevFailed &df )
  {
    m_dev_state = Tango::FAULT;
    m_dev_status = "Device is in error: proxy could not be initialised";
    ERROR_STREAM << "The following Tango exception was raised: " << df << endl;
    throw;
  }
  catch(...)
  {
    m_dev_state = Tango::FAULT;
    m_dev_status = "Device is in error: proxy could not be initialised";
    ERROR_STREAM << "An unknown exception was raised: " << endl;
    throw;
  }

  m_region_in_progress = 0;
  m_dev_state = Tango::STANDBY;
  m_dev_status = "Device is in STANDBY mode: Ready to accept commands";;
}

// Destructor

MultiRegionAcqTask::~MultiRegionAcqTask()
{
  if( m_target_proxy != NULL )
  {
    delete m_target_proxy;
  }
}

// ============================================================================
// MultiRegionAcqTask::process_message
// ============================================================================
void MultiRegionAcqTask::process_message (yat::Message& msg)
  throw (Tango::DevFailed)
{
  //- handle msg
  switch (msg.type())
  {
    //- THREAD_INIT ----------------------
    case yat::TASK_INIT:
      {
	      DEBUG_STREAM << "MultiRegionAcqTask::handle_message::THREAD_INIT::thread is starting up" << std::endl;
        this->enable_periodic_msg(true);
				this->set_periodic_msg_period(1000);
      }
	    break;

	  //- THREAD_EXIT ----------------------
	  case yat::TASK_EXIT:
	    {
			  DEBUG_STREAM << "MultiRegionAcqTask::handle_message::THREAD_EXIT::thread is quitting" << std::endl;
      }
		  break;

	  //- THREAD_PERIODIC ------------------
	  case yat::TASK_PERIODIC:
	    {
	      this->periodic_job();
      }
	    break;

	  //- THREAD_TIMEOUT -------------------
	  case yat::TASK_TIMEOUT:
	    {
        //- not used in this example
      }
      break;

    //- kSTART_PERIODIC_MSG --------------
    case kSTART_PERIODIC_MSG:
      {
    	  DEBUG_STREAM << "MultiRegionAcqTask::handle_message::kSTART_PERIODIC_MSG" << std::endl;
        this->enable_periodic_msg(true);
      }
      break;

    //- kSTOP_PERIODIC_MSG ---------------
    case kSTOP_PERIODIC_MSG:
      {
    	  DEBUG_STREAM << "MultiRegionAcqTask::handle_message::kSTOP_PERIODIC_MSG" << std::endl;
        this->enable_periodic_msg(false);
      }
      break;

    //- kSTART_ACQUISITION_MSG ---------------
    case kSTART_ACQUISITION_MSG:
    {
      DEBUG_STREAM << "MultiRegionAcqTask::handle_message::kSTART_ACQUISITION_MSG: " << std::endl;
      this->start_acquisition();
    }
    break;

    //- kSTOP_ACQUISITION_MSG ---------------
    case kSTOP_ACQUISITION_MSG:
    {
      DEBUG_STREAM << "MultiRegionAcqTask::handle_message::kSTOP_ACQUISITION_MSG: " << std::endl;
      this->stop_acquisition();
    }
    break;

    //- kRESET_CONFIG_MSG ----------------
    case kRESET_CONFIG_MSG:
    {
      DEBUG_STREAM << "MultiRegionAcqTask::handle_message::kRESET_CONFIG_MSG: " << std::endl;
      this->reset_configuration();
    }
    break;

    //- kVALIDATE_CONFIG_MSG ----------------
    case kVALIDATE_CONFIG_MSG:
    {
      DEBUG_STREAM << "MultiRegionAcqTask::handle_message::kVALIDATE_CONFIG_MSG: " << std::endl;
      this->validate_configuration();
    }
    break;

    //- UNHANDLED MSG --------------------
		default:
		  DEBUG_STREAM << "MultiRegionAcqTask::handle_message::unhandled msg type received  " << msg.type() << std::endl;
			break;
  }
}

void MultiRegionAcqTask::go (size_t tmo_ms)
    throw (Tango::DevFailed)
{
  //- a timer
  yat::Timer t;

  // Create dynamic atributes
  try
  {
    std::vector<yat4tango::DynamicAttributeInfo> dai_list;

    // Create scalar attributes
    for( size_t region_ind = 0; region_ind < m_max_region_num; region_ind++ )
    {
      for( size_t name_ind = 0; name_ind < 3; name_ind++ )
      {
        std::stringstream s;
        s << k_spect_attr_names[name_ind] << region_ind;
        yat4tango::DynamicAttributeInfo dai;
        dai.dev = m_host_device;
        dai.tai.name = s.str();
        dai.tai.data_format = Tango::SPECTRUM;
        dai.tai.data_type = Tango::DEV_DOUBLE;
        dai.tai.writable = Tango::READ;
        dai.tai.disp_level = Tango::OPERATOR;
        dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this,&MultiRegionAcqTask::spectrum_rd_cb);
        dai_list.push_back(dai);
      }

      // Create image attribute
      std::stringstream s;
      s << k_image_attr_name << region_ind;
      yat4tango::DynamicAttributeInfo dai;
      dai.dev = m_host_device;
      dai.tai.name = s.str();;
      dai.tai.data_format = Tango::IMAGE;
      dai.tai.data_type = Tango::DEV_DOUBLE;
      dai.tai.writable = Tango::READ;
      dai.tai.disp_level = Tango::OPERATOR;
      dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this,&MultiRegionAcqTask::image_rd_cb);
      dai_list.push_back(dai);
    }
    //- add all dynamic attributes to the device interface
    this->m_dam.add_attributes(dai_list);
  }
  catch (Tango::DevFailed & df)
  {
    ERROR_STREAM << df << std::endl;
    m_dev_state = Tango::FAULT;
    m_dev_status = "Device is in error: dynamic attributes initialization failed (see 'log' attribute for details)";
    RETHROW_DEVFAILED(df,
                      _CPTC("DEVICE_ERROR"),
                      _CPTC("dynamic attributes initialization failed [Tango exception caught]"),
                      _CPTC("MultiRegionAcqTask::go"));
  }
  catch (...)
  {
    m_dev_state = Tango::FAULT;
    m_dev_status = "Device is in error: dynamic attributes initialization failed (unknown exception caught)";
    THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
                    _CPTC("dynamic attributes initialization failed [unknown exception caught]"),
                    _CPTC("MultiRegionAcqTask::go"));
  }

  //- finally call yat4tango::DeviceTask::go
  long remaining_tmo = static_cast<long>(tmo_ms) - static_cast<long>(t.elapsed_msec());
  this->yat4tango::DeviceTask::go(remaining_tmo > 1 ? remaining_tmo : 1);
}

void MultiRegionAcqTask::exit()
    throw (Tango::DevFailed)
{
  //- stop the periodic activity
  try
  {
    this->stop_periodic_activity(3000);
  }
  catch (...)
  {
    //- ignore errors cause we need to exit!
  }

  //- removes all dynamic attributes from de device interface
  try
  {
    this->m_dam.remove_attributes();
  }
  catch (...)
  {
    //- ignore errors cause we need to exit!
  }

  // Clean any assigned data structures
  reset_data_vect();

  //- finally call call yat4tango::DeviceTask::exit
  this->yat4tango::DeviceTask::exit();
}

// Read callback for spectrum dynamic attributes
// will be called when a spectrum attribute is read

void MultiRegionAcqTask::spectrum_rd_cb (yat4tango::DynamicAttributeReadCallbackData& d)
  throw (Tango::DevFailed)
{
  if( m_data_has_changed == true )
  {
    std::string attr_name = d.tga->get_name();
    std::string str_name, str_ind;
    long attr_ind;
    size_t pos;

    DEBUG_STREAM << "attr_name = " << attr_name << endl;

    pos = attr_name.find_last_of( "R" );

    if( pos != std::string::npos )
    {
      str_name = attr_name.substr( 0, pos );
      str_ind = attr_name.substr( pos + 1, attr_name.size() - pos - 1 );
      attr_ind = atoi( str_ind.c_str() );

      DEBUG_STREAM << "attr_ind = " << attr_ind << endl;

    }
    else
    {
      ERROR_STREAM << "Attribute name could not be parsed" << attr_name << std::endl;
      m_dev_state = Tango::FAULT;
      m_dev_status = "Device is in error: Attribute name could not be parsed (see 'log' attribute for details)";

      THROW_DEVFAILED(_CPTC("DEVICE_ERROR"),
                      _CPTC("Attribute name could not be parsed"),
                      _CPTC("MultiRegionAcqTask::spectrum_rd_cb"));
    }

    DEBUG_STREAM << "MultiRegionAcqTask::spectrum_rd_cb::reading attr. id " << attr_ind << std::endl;

    if( ( attr_ind >= m_config.num_regions )
      || ( m_channel_scale_array.size() <= attr_ind )
      || ( m_slice_scale_array.size() <= attr_ind )
      || ( m_sum_data_array.size() <= attr_ind )
      || ( m_data_array.size() <= attr_ind ) )
    {
      THROW_DEVFAILED(_CPTC("NO_DATA"),
                      _CPTC("No data available for the specified region"),
                      _CPTC("MultiRegionAcqTask::spectrum_rd_cb"));
    }
    else
    {

      { //- critical section: in

      yat::AutoMutex<> guard(this->m_data_lock);

      if( str_name == "channelScale" )
      {
        if( m_channel_scale_array[attr_ind] != NULL )
        {
          d.tga->set_value( (*m_channel_scale_array[attr_ind]).get_buffer(), (*m_channel_scale_array[attr_ind]).length() );
        }
      }
      else if( str_name == "sliceScale" )
      {
        if( m_slice_scale_array[attr_ind] != NULL )
        {
          d.tga->set_value( (*m_slice_scale_array[attr_ind]).get_buffer(), (*m_slice_scale_array[attr_ind]).length() );
        }
      }
      else if( str_name == "sumData" )
      {
        if( m_sum_data_array[attr_ind] != NULL )
        {
          d.tga->set_value( (*m_sum_data_array[attr_ind]).get_buffer(), (*m_sum_data_array[attr_ind]).length() );
        }
      }
      else
      {
        ERROR_STREAM << "Attribute name could not be recognised" << str_name << std::endl;
        m_dev_state = Tango::FAULT;
        m_dev_status = "Device is in error: Attribute name could not be recognised (see 'log' attribute for details)";

        THROW_DEVFAILED(_CPTC("IRREGULAR_ATTRIBUTE"),
                        _CPTC("Attribute name could not be recognised"),
                        _CPTC("MultiRegionAcqTask::spectrum_rd_cb"));
      }
      } //- critical section: out

    }
  }
  else
  {
      THROW_DEVFAILED(_CPTC("NO_DATA"),
                      _CPTC("No data available for the specified region"),
                      _CPTC("MultiRegionAcqTask::spectrum_rd_cb"));
  }
}

// Read callback for image dynamic attributes
// will be called when an image attribute is read

void MultiRegionAcqTask::image_rd_cb (yat4tango::DynamicAttributeReadCallbackData& d)
  throw (Tango::DevFailed)
{
  if( m_data_has_changed == true )
  {
    std::string attr_name = d.tga->get_name();
    std::string str_name, str_ind;
    long attr_ind;
    size_t pos;

    pos = attr_name.find_last_of( "R" );

    if( pos != std::string::npos )
    {
      str_name = attr_name.substr( 0, pos );
      str_ind = attr_name.substr( pos + 1, attr_name.size() - pos - 1 );
      attr_ind = atoi( str_ind.c_str() );
    }
    else
    {
      ERROR_STREAM << "Attribute name could not be parsed" << attr_name << std::endl;
      m_dev_state = Tango::FAULT;
      m_dev_status = "Device is in error: Attribute name could not be parsed (see 'log' attribute for details)";

      THROW_DEVFAILED(_CPTC("IRREGULAR_ATTRIBUTE"),
                      _CPTC("Attribute name could not be parsed"),
                      _CPTC("MultiRegionAcqTask::image_rd_cb"));
    }

    DEBUG_STREAM << "MultiRegionAcqTask::image_rd_cb::reading attr. id " << attr_ind << std::endl;

    if( attr_ind >= m_config.num_regions )
    {
      THROW_DEVFAILED(_CPTC("NO_DATA"),
                      _CPTC("No data available for the specified region"),
                      _CPTC("MultiRegionAcqTask::image_rd_cb"));
    }
    else
    {
      { //- critical section: in

      yat::AutoMutex<> guard(this->m_data_lock);

      if( str_name == "data" )
      {
        if( m_data_array[attr_ind] != NULL )
        {
          d.tga->set_value( (*m_data_array[attr_ind]).get_buffer(), m_channels, m_slices );
        }
      }
      else
      {
        ERROR_STREAM << "Attribute name could not be recognised" << str_name << std::endl;
        m_dev_state = Tango::FAULT;
        m_dev_status = "Device is in error: Attribute name could not be recognised (see 'log' attribute for details)";

        THROW_DEVFAILED(_CPTC("IRREGULAR_ATTRIBUTE"),
                        _CPTC("Attribute name could not be recognised"),
                        _CPTC("MultiRegionAcqTask::image_rd_cb"));
      }
      } //- critical section: out
    }
  }
  else
  {
      THROW_DEVFAILED(_CPTC("NO_DATA"),
                      _CPTC("No data available for the specified region"),
                      _CPTC("MultiRegionAcqTask::image_rd_cb"));
  }
}

void MultiRegionAcqTask::start_cmd()
{
  m_dev_state = Tango::RUNNING;

  this->post( kSTART_ACQUISITION_MSG );
}

void MultiRegionAcqTask::stop_cmd()
{
  this->post( kSTOP_ACQUISITION_MSG );
}

void MultiRegionAcqTask::validate_cfg_cmd()
{
  this->wait_msg_handled( kVALIDATE_CONFIG_MSG );
}

void MultiRegionAcqTask::reset_cfg_cmd()
{
  this->post( kRESET_CONFIG_MSG );
}

void MultiRegionAcqTask::reset_configuration()
{
  m_config.num_regions = 0;
  m_config.initialised = false;
  m_config.region_attrs.clear();
}

// validate_configuration: Check configration validity
//
void MultiRegionAcqTask::validate_configuration()
{
  bool result = true;
  std::string reason;

  // Check that actual region number is less than max region number
  if( m_config.num_regions > m_max_region_num )
  {
    reason = "Number of regions greater than max number of regions";
    result = false;
  }

	// check taht region number is not zero

	else if( m_config.num_regions == 0 )
  {
    reason = "Number of regions is zero (configuration may not be initialised)";
    result = false;
  }
  else if( strcmp( m_config.pass_mode, DEFAULT_CONFIG_STR ) == 0 )
  {
    reason = "Atribute pass_mode is not initialised";
    result = false;
  }

  // Check that spectrum attributes have the correct size (= number of regions)

  else if( m_lens_mode_array.length() != m_config.num_regions )
  {
    reason = "Incorrect size of lens mode data";
    result = false;
  }
  else if( m_pass_energy_array.length() != m_config.num_regions )
  {
    reason = "Incorrect size of pass energy data";
    result = false;
  }
  else if( m_fix_energy_array.length() != m_config.num_regions )
  {
    reason = "Incorrect size of fix energy data (fixed mode)";
    result = false;
  }
  else if( m_low_energy_array.length() != m_config.num_regions )
  {
    reason = "Incorrect size of low energy data (swept mode)";
    result = false;
  }
  else if( m_high_energy_array.length() != m_config.num_regions )
  {
    reason = "Incorrect size of high energy data (swept mode)";
    result = false;
  }
  else if( m_energy_step_array.length() != m_config.num_regions )
  {
    reason = "Incorrect size of energy step data (swept mode)";
    result = false;
  }
  else if( m_step_time_array.length() != m_config.num_regions )
  {
    reason = "Incorrect size of step time data (swept mode)";
    result = false;
  }

  if( result == false )
  {
    THROW_DEVFAILED("VALIDATION_ERROR",
                    reason.c_str(),
                    "MultiRegionAcqTask::validate_configuration");
  }
}

// init_region_configuration
// Fill region configuration structure with the contants of vectors supplied by caller device

void MultiRegionAcqTask::init_region_configuration()
{
  size_t region_ind;
	m_config.region_attrs.clear();

  for( region_ind = 0; region_ind < m_config.num_regions; region_ind++ )
  {
    RegionConfig reg_cfg;

    reg_cfg.mode = m_mode_array[region_ind];
    reg_cfg.low_energy = m_low_energy_array[region_ind];
    reg_cfg.high_energy = m_high_energy_array[region_ind];
    reg_cfg.fix_energy = m_fix_energy_array[region_ind];
    reg_cfg.energy_step = m_energy_step_array[region_ind];
    reg_cfg.step_time = m_step_time_array[region_ind];
    reg_cfg.lens_mode = m_lens_mode_array[region_ind];
    reg_cfg.pass_energy = m_pass_energy_array[region_ind];

    m_config.region_attrs.push_back( reg_cfg );
  }

  m_config.initialised = true;
}

// start_acquisition
// Launch an acquisition on the ScientaAcquisition device
void MultiRegionAcqTask::start_acquisition()
  throw (Tango::DevFailed)
{
  std::string reason;
  reason.clear();

  // Clean data structures that will receive acquisition results
  reset_data_vect();

  // _ config
  try
  {
    validate_configuration();
  }
  catch( Tango::DevFailed &df )
  {
    m_dev_state = Tango::FAULT;
    m_dev_status = "Device is in FAULT state - invalid configuration - see 'log' attribute for details";
    ERROR_STREAM << df << std::endl;
    RETHROW_DEVFAILED(  df,
                    "VALIDATION_ERROR",
                    "Cnfiguration validation failure",
                    "MultiRegionAcqTask::start_acquisition");
  }

  // if config OK init config
  init_region_configuration();

  vector<Tango::DeviceAttribute> acq_attrs;
  acq_attrs.clear();
  write_common_attrs( acq_attrs );
  write_specific_attrs( 0, acq_attrs );

  // send general and region-specific attribute to the ScientaAcquisition device
  try
  {
    m_target_proxy->write_attributes( acq_attrs );
  }
  catch( Tango::DevFailed &df )
  {
    m_dev_state = Tango::FAULT;
    m_dev_status = "Device is in FAULT state: Attribute write failed (see 'log' attribute for details)";
    ERROR_STREAM << df << std::endl;
    RETHROW_DEVFAILED( df,
                    "ATTRIBUTE_WRITE_ERROR",
                    "Tango exception occured while writing attributes",
                    "MultiRegionAcqTask::start_acquisition");
  }
  catch(...)
  {
    m_dev_state = Tango::FAULT;
    m_dev_status = "Device is in FAULT state: Attribute write failed (unknown exception caught)";
    THROW_DEVFAILED("ATTRIBUTE_WRITE_ERROR",
                    "Unknown exception caught writing attributes",
                    "MultiRegionAcqTask::start_acquisition");
  }

  // start acquisition for first region
  try
  {
    m_target_proxy->command_inout( "Start" );
  }
  catch( Tango::DevFailed &df )
  {
    ERROR_STREAM << df << std::endl;
    m_dev_state = Tango::FAULT;
    m_dev_status = "Device is in FAULT state: Start acquisition failed (see 'log' attribute for details)";
    RETHROW_DEVFAILED( df,
                    "START_ACQ_ERROR",
                    "Tango exception occured while starting acquisition",
                    "MultiRegionAcqTask::start_acquisition");
  }
  catch(...)
  {
    m_dev_state = Tango::FAULT;
    m_dev_status = "Device is in FAULT state: Start acquisition failed (unknown exception caught)";
    THROW_DEVFAILED("START_ACQ_ERROR",
                    "Unknown excezption caught while starting acquisition",
                    "MultiRegionAcqTask::start_acquisition");
  }

  // if start OK  set region in progress
  //              set state = RUNNING
  m_region_in_progress = 0;
  m_dev_state = Tango::RUNNING;
  m_dev_status = "Device is in up and running: performing acquisition";

	yat::Timeout t( 3, yat::Timeout::TMO_UNIT_SEC, true );

	Tango::DevState scienta_state = Tango::STANDBY;

	while ( scienta_state != Tango::RUNNING )
	{
    if ( t.expired() )
		{
    	THROW_DEVFAILED("START_ACQ_ERROR",
      	              "Timeout expred while waiting for the ACQ to start on ScientaAcqusition side",
      	              "MultiRegionAcqTask::start_acquisition");
		}
		scienta_state = m_target_proxy->state();
	}

	this->set_periodic_msg_period(100);
}

void MultiRegionAcqTask::stop_acquisition()
  throw (Tango::DevFailed)
{
    // start acquisition for first region
    try
    {
      m_target_proxy->command_inout( "Stop" );
    }
    catch( Tango::DevFailed &df )
    {
      ERROR_STREAM << df << std::endl;
      m_dev_state = Tango::FAULT;
      m_dev_status = "Device is in FAULT state: Stop acquisition failed (see 'log' attribute for details)";
      RETHROW_DEVFAILED( df,
                      "STOP_ACQ_ERROR",
                      "Tango exception occured while stopping acquisition",
                      "MultiRegionAcqTask::stop_acquisition");
    }
    catch(...)
    {
      m_dev_state = Tango::FAULT;
      m_dev_status = "Device is in FAULT state: Stop acquisition failed (unknown exception caught)";
      THROW_DEVFAILED("STOP_ACQ_ERROR",
                      "Unknown excezption caught while stopping acquisition",
                      "MultiRegionAcqTask::stop_acquisition");
    }

    m_dev_state = Tango::ALARM;
    m_dev_status = "Device is in ALARM state: Acquisition was stopped";
}

// periodic_job
// periodic task: Check the ScientaAcquisition device state
// and retriev acquisition results when they are available

void MultiRegionAcqTask::periodic_job()
  throw (Tango::DevFailed)
{
  Tango::DevVarDoubleArray *read_channel_scale;
  Tango::DevVarDoubleArray *read_slice_scale;
  Tango::DevVarDoubleArray *read_sum_data;
  Tango::DevVarDoubleArray *read_data;

  // Do something only if ScientaMulti state = RUNNING
  // i.e. if an acqusition is in progress
  if( m_dev_state == Tango::RUNNING )
  {
    Tango::DevState scienta_acq_state;

    // Check ScientaAcquisiton device state
    try
    {
      scienta_acq_state = m_target_proxy->state();
    }
    catch ( Tango::DevFailed &df )
    {
      ERROR_STREAM << df << std::endl;
      m_dev_state = Tango::FAULT;
      m_dev_status = "Device is in FAULT state: Could not get ScientaAcquisition device state (see 'log' attribute for details)";
      RETHROW_DEVFAILED( df,
                      "STATE_READ_ERROR",
                      "Tango exception caught while getting ScientaAcqusition state",
                      "MultiRegionAcqTask::periodic_job");
    }
    catch(...)
    {
      m_dev_state = Tango::FAULT;
      m_dev_status = "Device is in FAULT state: Could not get ScientaAcquisition device state (unknown exception caught)";
      THROW_DEVFAILED("STATE_READ_ERROR",
                      "Unknown excezption caught while getting ScientaAcqusition state",
                      "MultiRegionAcqTask::periodic_job");
    }

    switch( scienta_acq_state )
    {
      case SACQ_RUNNING_STATE:
        // Acquisition is in progress --> do nothing
        break;
      case SACQ_STANDBY_STATE:
      {
        // get acquisition results
        vector<Tango::DeviceAttribute> *devattr;
        vector<string> attr_names;
        Tango::DevLong slices, channels;
        size_t attr_ind = 0;


        attr_names.push_back( "slices" );
        attr_names.push_back( "channels" );
        attr_names.push_back( "channelScale" );
        attr_names.push_back( "sliceScale" );
        attr_names.push_back( "sumData" );
        attr_names.push_back( "data" );

        try
        {
          devattr = m_target_proxy->read_attributes( attr_names );
        }
        catch( Tango::DevFailed &df )
        {
          stringstream s;

          ERROR_STREAM << df << std::endl;
          m_dev_state = Tango::FAULT;
          m_dev_status = "Device is in FAULT state: Could not read acquisition results (see 'log' attribute for details)";
          s << "Tango exception caught reading results for region " << m_region_in_progress;
          RETHROW_DEVFAILED( df,
                          "ATTR_READ_ERROR",
                          s.str().c_str(),
                          "MultiRegionAcqTask::periodic_job");
        }
        catch(...)
        {
          stringstream s;

          m_dev_state = Tango::FAULT;
          m_dev_status = "Device is in FAULT state: Could not read acquisition results (see 'log' attribute for details)";
          s << "Unknown exception caugnt reading results for region " << m_region_in_progress;
          THROW_DEVFAILED("ATTR_READ_ERROR",
                          s.str().c_str(),
                          "MultiRegionAcqTask::periodic_job");
        }

        if( (*devattr)[attr_ind] >> m_slices == false )
        {
          m_dev_state = Tango::FAULT;
          m_dev_status = "Device is in FAULT state: Failed to extract slices attribute";
          THROW_DEVFAILED(_CPTC ("INTERNAL_ERROR"),
                         _CPTC ("Failed to extract from DeviceAttribute"),
                         _CPTC ("MultiRegionAcqTask::periodic_job"));
        }
        attr_ind++;

        if( (*devattr)[attr_ind] >> m_channels == false )
        {
          m_dev_state = Tango::FAULT;
          m_dev_status = "Device is in FAULT state: Failed to extract channels attribute";
          THROW_DEVFAILED(_CPTC ("INTERNAL_ERROR"),
                         _CPTC ("Failed to extract from DeviceAttribute"),
                         _CPTC ("MultiRegionAcqTask::periodic_job"));
        }
        attr_ind++;

        { //- critical section: in

        yat::AutoMutex<> guard(this->m_data_lock);

        // read channelScale spectrum attibute

        read_channel_scale = new Tango::DevVarDoubleArray;
        read_channel_scale->length( m_channels );

        if( (*devattr)[attr_ind] >> read_channel_scale == false )
        {
          m_dev_state = Tango::FAULT;
          m_dev_status = "Device is in FAULT state: Failed to extract channelScale attribute";
          THROW_DEVFAILED(_CPTC ("INTERNAL_ERROR"),
                         _CPTC ("Failed to extract from DeviceAttribute"),
                         _CPTC ("MultiRegionAcqTask::periodic_job"));
        }
        else
        {
          m_channel_scale_array.push_back( read_channel_scale );
        }

        attr_ind++;

        // read sliceSacle spectrum attribute
        read_slice_scale = new Tango::DevVarDoubleArray;
        read_slice_scale->length( m_slices );

        if( (*devattr)[attr_ind] >> read_slice_scale == false )
        {
          m_dev_state = Tango::FAULT;
          m_dev_status = "Device is in FAULT state: Failed to extract sliceScale attribute";
          THROW_DEVFAILED(_CPTC ("INTERNAL_ERROR"),
                         _CPTC ("Failed to extract from DeviceAttribute"),
                         _CPTC ("MultiRegionAcqTask::periodic_job"));
        }
        else
        {
          // m_slice_scale_array[m_region_in_progress] = read_slice_scale;
          m_slice_scale_array.push_back( read_slice_scale );
        }

        attr_ind++;

        // read sumData spectrum attribute
        read_sum_data = new Tango::DevVarDoubleArray;
        read_sum_data->length( m_channels );

        if( (*devattr)[attr_ind] >> read_sum_data == false )
        {
          m_dev_state = Tango::FAULT;
          m_dev_status = "Device is in FAULT state: Failed to extract sumData attribute";
          THROW_DEVFAILED(_CPTC ("INTERNAL_ERROR"),
                         _CPTC ("Failed to extract from DeviceAttribute"),
                         _CPTC ("MultiRegionAcqTask::periodic_job"));
        }
        else
        {
          //m_sum_data_array[m_region_in_progress] = read_sum_data;
          m_sum_data_array.push_back( read_sum_data );
        }

        attr_ind++;

        // read data image attribute
        read_data = new Tango::DevVarDoubleArray;
        read_data->length( m_channels * m_slices );

        if( (*devattr)[attr_ind] >> read_data == false )
        {
          m_dev_state = Tango::FAULT;
          m_dev_status = "Device is in FAULT state: Failed to extract data attribute";
          THROW_DEVFAILED(_CPTC ("INTERNAL_ERROR"),
                         _CPTC ("Failed to extract from DeviceAttribute"),
                         _CPTC ("MultiRegionAcqTask::periodic_job"));
        }
        else
        {
          // m_data_array[m_region_in_progress] = read_data;

          m_data_array.push_back( read_data );
        }

        } //- critical section: out

        delete devattr;
        // if more regions to acquire

        if( m_region_in_progress != m_config.num_regions - 1 )
        {
            // start new acquisition
          vector<Tango::DeviceAttribute> acq_attrs;
          write_specific_attrs( m_region_in_progress + 1, acq_attrs );

          try
          {
            m_target_proxy->write_attributes( acq_attrs );
          }
          catch( Tango::DevFailed &df )
          {
            ERROR_STREAM << df << std::endl;
            m_dev_state = Tango::FAULT;
            m_dev_status = "Device is in FAULT state: Failed to write acquisition results (see 'log' attribute for details)";
            RETHROW_DEVFAILED( df,
                            "ATTRIBUTE_WRITE_ERROR",
                            "Tango exception occured while writing attributes",
                            "MultiRegionAcqTask::start_acquisition");
          }
          catch(...)
          {
            m_dev_state = Tango::FAULT;
            m_dev_status = "Device is in FAULT state: Failed to write acquisition results (unknown exception caught)";
            THROW_DEVFAILED("ATTRIBUTE_WRITE_ERROR",
                            "Unknown excezption caught writing attributes",
                            "MultiRegionAcqTask::start_acquisition");
          }

          // start acquisition for next region
          try
          {
            m_target_proxy->command_inout( "start" );
          }
          catch( Tango::DevFailed &df )
          {
            ERROR_STREAM << df << std::endl;
            m_dev_state = Tango::FAULT;
            m_dev_status = "Device is in FAULT state: Failed to start acquisition (see 'log' attribute for details)";
            RETHROW_DEVFAILED( df,
                            "START_ACQ_ERROR",
                            "Tango exception occured while starting acquisition",
                            "MultiRegionAcqTask::start_acquisition");
          }
          catch(...)
          {
            m_dev_state = Tango::FAULT;
            m_dev_status = "Device is in FAULT state: Failed to start acquisition (unknown exception caught)";
            THROW_DEVFAILED("START_ACQ_ERROR",
                            "Unknown excezption caught while starting acquisition",
                            "MultiRegionAcqTask::start_acquisition");
          }

					yat::Timeout t( 3, yat::Timeout::TMO_UNIT_SEC, true );

					scienta_acq_state = Tango::STANDBY;

					while ( scienta_acq_state != Tango::RUNNING )
					{
    				if ( t.expired() )
						{
              m_dev_state = Tango::FAULT;
              m_dev_status = "Device is in FAULT state: Timeout expired while waiting for the ACQ to start on ScientaAcqusition side";

    					THROW_DEVFAILED("START_ACQ_ERROR",
      	              				"Timeout expred while waiting for the ACQ to start on ScientaAcqusition side",
      	              			  "MultiRegionAcqTask::start_acquisition");
						}
						scienta_acq_state = m_target_proxy->state();
					}

          // update region in progress
          m_region_in_progress++;
        }
        else
        {
          // else
          // finish acquisition
          m_dev_state = Tango::STANDBY;
          m_dev_status = "Device is in STANDBY state: Waiting for commands";
          m_data_has_changed = true;
					this->set_periodic_msg_period(1000);
        }
        break;
      }
      case SACQ_FAULT_STATE:
      {
				this->set_periodic_msg_period(1000);
        stringstream s;
        m_dev_state = Tango::FAULT;
        m_dev_status = "Device is in FAULT state: Acquisition failed on ScientaAcquisition device";
        s << "Acquisition failed for region " << m_region_in_progress << endl;
        THROW_DEVFAILED("STATE_READ_ERROR",
                        s.str().c_str(),
                        "MultiRegionAcqTask::periodic_job");

        break;
      }
      default:
        break;
    }
  }
}

// ============================================================================
// GroupManagerTask::start_periodic_activity
// ============================================================================
void MultiRegionAcqTask::start_periodic_activity (size_t tmo_ms)
    throw (Tango::DevFailed)
{
  try
  {
    //- synchronous approach: "post then wait for the message to be handled"
    if (tmo_ms)
      this->wait_msg_handled(kSTART_PERIODIC_MSG, tmo_ms);
    //- asynchronous approach: "post the data then returm immediately"
    else
      this->post(kSTART_PERIODIC_MSG);
  }
  catch (const Tango::DevFailed&)
  {
    //- an exception could be thrown if the task msgQ is full (high water mark reached)
    //- in the synchronous case we could also caught an exception thrown by the code
    //- handling the message
    throw;
  }
}

// ============================================================================
// GroupManagerTask::stop_periodic_activity
// ============================================================================
void MultiRegionAcqTask::stop_periodic_activity (size_t tmo_ms)
    throw (Tango::DevFailed)
{
  try
  {
    //- synchronous approach: "post then wait for the message to be handled"
    if (tmo_ms)
      this->wait_msg_handled(kSTOP_PERIODIC_MSG, tmo_ms);
    //- asynchronous approach: "post the data then returm immediately"
    else
      this->post(kSTOP_PERIODIC_MSG);
  }
  catch (const Tango::DevFailed&)
  {
    //- an exception could be thrown if the task msgQ is full (high water mark reached)
    //- in the synchronous case we could also caught an exception thrown by the code
    //- handling the message
    throw;
  }
}

// write_common_attrs
// Fill DeviceAttribute vector with common acqusition attributes
// Data in: the vector to be filled

void MultiRegionAcqTask::write_common_attrs( vector<Tango::DeviceAttribute> &attr_vect )
{
  attr_vect.push_back( Tango::DeviceAttribute( "excitationEnergy", m_config.excitation_energy ) );
#if (TANGO_VERSION_MAJOR >= 9)
  attr_vect.push_back( Tango::DeviceAttribute( "passMode", (const char*)(m_config.pass_mode) ) );
#else
  attr_vect.push_back( Tango::DeviceAttribute( "passMode", m_config.pass_mode ) );
#endif
  attr_vect.push_back( Tango::DeviceAttribute( "detectorFirstXChannel", m_config.detector_first_x_channel ) );
  attr_vect.push_back( Tango::DeviceAttribute( "detectorLastXChannel", m_config.detector_last_x_channel ) );
  attr_vect.push_back( Tango::DeviceAttribute( "detectorFirstYChannel", m_config.detector_first_y_channel ) );
  attr_vect.push_back( Tango::DeviceAttribute( "detectorLastYChannel", m_config.detector_last_y_channel ) );
  attr_vect.push_back( Tango::DeviceAttribute( "detectorSlices", m_config.detector_slices ) );
  attr_vect.push_back( Tango::DeviceAttribute( "ADCMode", m_config.ADC_mode ) );
  if ( m_config.has_ADC_mask_support)
    attr_vect.push_back( Tango::DeviceAttribute( "ADCMask", m_config.ADC_mask ) );
  if ( m_config.has_ADC_discriminator_support )
    attr_vect.push_back( Tango::DeviceAttribute( "discriminatorLevel", m_config.discriminator_level ) );
}

// write_specific_attrs
// Fill DeviceAttribute vector with region-specific acqusition attributes
// Data in: region number
// Data in: the vector to be filled

void MultiRegionAcqTask::write_specific_attrs( int region_ind, vector<Tango::DeviceAttribute> &attr_vect )
{
#if (TANGO_VERSION_MAJOR >= 9)
  attr_vect.push_back( Tango::DeviceAttribute( "mode", (const char*)(m_config.region_attrs[region_ind].mode) ) );
  attr_vect.push_back( Tango::DeviceAttribute( "lensMode", (const char*)(m_config.region_attrs[region_ind].lens_mode) ) );
#else
  attr_vect.push_back( Tango::DeviceAttribute( "mode", m_config.region_attrs[region_ind].mode ) );
  attr_vect.push_back( Tango::DeviceAttribute( "lensMode", m_config.region_attrs[region_ind].lens_mode ) );
#endif

  attr_vect.push_back( Tango::DeviceAttribute( "passEnergy", m_config.region_attrs[region_ind].pass_energy ) );

  if( strcmp( m_config.region_attrs[region_ind].mode, "Swept" ) == 0 )
  {
    attr_vect.push_back( Tango::DeviceAttribute( "highEnergy", m_config.region_attrs[region_ind].high_energy ) );
    attr_vect.push_back( Tango::DeviceAttribute( "lowEnergy", m_config.region_attrs[region_ind].low_energy ) );
    attr_vect.push_back( Tango::DeviceAttribute( "energyStep", m_config.region_attrs[region_ind].energy_step ) );
    attr_vect.push_back( Tango::DeviceAttribute( "stepTime", m_config.region_attrs[region_ind].step_time ) );
  }
  else if( strcmp( m_config.region_attrs[region_ind].mode, "Fixed" ) == 0 )
  {
    attr_vect.push_back( Tango::DeviceAttribute( "fixEnergy", m_config.region_attrs[region_ind].fix_energy ) );
    attr_vect.push_back( Tango::DeviceAttribute( "stepTime", m_config.region_attrs[region_ind].step_time ) );
  }
}

// Accessors for acquisition attributes
void MultiRegionAcqTask::set_region_num( Tango::DevLong reg_num )
{
  m_config.num_regions = reg_num;
}

Tango::DevLong MultiRegionAcqTask::get_region_num()
{
  return m_config.num_regions;
}

Tango::DevULong MultiRegionAcqTask::get_region_in_progress()
{
  return m_region_in_progress;
}

void MultiRegionAcqTask::set_detector_slices( Tango::DevShort det_sli )
{
  m_config.detector_slices = det_sli;
}

Tango::DevShort MultiRegionAcqTask::get_detector_slices()
{
   return m_config.detector_slices;
}

Tango::DevVarDoubleArray& MultiRegionAcqTask::read_energy_step()
{
  return m_energy_step_array;
}

void MultiRegionAcqTask::write_energy_step( const Tango::DevDouble *double_data, int len )
{
  m_energy_step_array.length( len );

  for( size_t reg_ind = 0; reg_ind < len; reg_ind++ )
  {
    m_energy_step_array[reg_ind] = double_data[reg_ind];
  }
}

void MultiRegionAcqTask::set_excitation_energy( Tango::DevDouble exc_enr )
{
  m_config.excitation_energy = exc_enr;
}

Tango::DevDouble MultiRegionAcqTask::get_excitation_energy()
{
  return m_config.excitation_energy;
}

void MultiRegionAcqTask::set_pass_mode( Tango::DevString pass_mode )
{
  m_config.pass_mode = pass_mode;
}

Tango::DevString MultiRegionAcqTask::get_pass_mode()
{
  return m_config.pass_mode;
}

void MultiRegionAcqTask::set_detector_first_x_channel( Tango::DevShort first_x_chan )
{
  m_config.detector_first_x_channel = first_x_chan;
}

Tango::DevShort MultiRegionAcqTask::get_detector_first_x_channel()
{
  return m_config.detector_first_x_channel;
}

void MultiRegionAcqTask::set_detector_last_x_channel( Tango::DevShort last_x_chan )
{
  m_config.detector_last_x_channel = last_x_chan;
}

Tango::DevShort MultiRegionAcqTask::get_detector_last_x_channel()
{
  return m_config.detector_last_x_channel;
}

void MultiRegionAcqTask::set_detector_first_y_channel( Tango::DevShort first_y_chan )
{
  m_config.detector_first_y_channel = first_y_chan;
}

Tango::DevShort MultiRegionAcqTask::get_detector_first_y_channel()
{
  return m_config.detector_first_y_channel;
}

void MultiRegionAcqTask::set_detector_last_y_channel( Tango::DevShort last_y_chan )
{
  m_config.detector_last_y_channel = last_y_chan;
}

Tango::DevShort MultiRegionAcqTask::get_detector_last_y_channel()
{
  return m_config.detector_last_y_channel;
}

Tango::DevVarDoubleArray& MultiRegionAcqTask::read_high_energy()
{
  return m_high_energy_array;
}

void MultiRegionAcqTask::set_ADC_mode( Tango::DevShort adc_mode )
{
   m_config.ADC_mode = adc_mode;
}

Tango::DevShort MultiRegionAcqTask::get_ADC_mode()
{
  return m_config.ADC_mode;
}

void MultiRegionAcqTask::set_ADC_mask( Tango::DevShort adc_mask )
{
   m_config.ADC_mask = adc_mask;
}

Tango::DevShort MultiRegionAcqTask::get_ADC_mask()
{
  return m_config.ADC_mask;
}

void MultiRegionAcqTask::set_discriminator_level( Tango::DevShort disc_lvl )
{
  m_config.discriminator_level = disc_lvl;
}

Tango::DevShort MultiRegionAcqTask::get_discriminator_level()
{
  return m_config.discriminator_level;
}

void MultiRegionAcqTask::write_high_energy( const Tango::DevDouble *double_data, int len )
{
  DEBUG_STREAM << "write_high_energy len = " << len << endl;

  m_high_energy_array.length( len );

  for( size_t reg_ind = 0; reg_ind < len; reg_ind++ )
  {
    m_high_energy_array[reg_ind] = double_data[reg_ind];
  }

  DEBUG_STREAM << "m_high_energy_array length = " << m_high_energy_array.length() << endl;

}

Tango::DevVarDoubleArray& MultiRegionAcqTask::read_low_energy()
{
  return m_low_energy_array;
}

void MultiRegionAcqTask::write_low_energy( const Tango::DevDouble *double_data, int len )
{
  m_low_energy_array.length( len );

  for( size_t reg_ind = 0; reg_ind < len; reg_ind++ )
  {
    m_low_energy_array[reg_ind] = double_data[reg_ind];
  }
}

Tango::DevVarDoubleArray& MultiRegionAcqTask::read_fix_energy()
{
  return m_fix_energy_array;
}

void MultiRegionAcqTask::write_fix_energy( const Tango::DevDouble *double_data, int len )
{
  m_fix_energy_array.length( len );

  for( size_t reg_ind = 0; reg_ind < len; reg_ind++ )
  {
    m_fix_energy_array[reg_ind] = double_data[reg_ind];
  }
}

Tango::DevVarDoubleArray& MultiRegionAcqTask::read_step_time()
{
  return m_step_time_array;
}

void MultiRegionAcqTask::write_step_time( const Tango::DevDouble *double_data, int len )
{
  m_step_time_array.length( len );

  for( size_t reg_ind = 0; reg_ind < len; reg_ind++ )
  {
    m_step_time_array[reg_ind] = double_data[reg_ind];
  }
}

Tango::DevVarStringArray& MultiRegionAcqTask::read_lens_mode()
{
  return m_lens_mode_array;
}

void MultiRegionAcqTask::write_lens_mode( const Tango::ConstDevString *str_data, int len )
{
  m_lens_mode_array.length( len );

  for( size_t reg_ind = 0; reg_ind < len; reg_ind++ )
  {
    m_lens_mode_array[reg_ind] = str_data[reg_ind];
    DEBUG_STREAM << "MultiRegionAcqTask::write_lens_mode::" << m_lens_mode_array[reg_ind] << std::endl;
  }
}

Tango::DevVarStringArray& MultiRegionAcqTask::read_mode()
{
  return m_mode_array;
}

void MultiRegionAcqTask::write_mode( const Tango::ConstDevString *str_data, int len )
{
  m_mode_array.length( len );

  for( size_t reg_ind = 0; reg_ind < len; reg_ind++ )
  {
    m_mode_array[reg_ind] = str_data[reg_ind];
  }
}

Tango::DevVarDoubleArray& MultiRegionAcqTask::read_pass_energy()
{
  return m_pass_energy_array;
}

void MultiRegionAcqTask::write_pass_energy( const Tango::DevDouble *double_data, int len )
{
  m_pass_energy_array.length( len );

  for( size_t reg_ind = 0; reg_ind < len; reg_ind++ )
  {
    m_pass_energy_array[reg_ind] = double_data[reg_ind];
  }
}

Tango::DevState MultiRegionAcqTask::get_state()
{
  return m_dev_state;
}

std::string MultiRegionAcqTask::get_status()
{
  return m_dev_status;
}

void MultiRegionAcqTask::reset_data_vect()
{
  size_t data_ind;

  for( data_ind = 0; data_ind < m_channel_scale_array.size(); data_ind++ )
  {
    if( m_channel_scale_array[data_ind] != NULL )
    {
      delete m_channel_scale_array[data_ind];
      m_channel_scale_array[data_ind] = NULL;
    }
    m_channel_scale_array.clear();
  }

  for( data_ind = 0; data_ind < m_slice_scale_array.size(); data_ind++ )
  {
    if( m_slice_scale_array[data_ind] != NULL )
    {
      delete m_slice_scale_array[data_ind];
      m_slice_scale_array[data_ind] = NULL;
    }
    m_slice_scale_array.clear();
  }

  for( data_ind = 0; data_ind < m_sum_data_array.size(); data_ind++ )
  {
    if( m_sum_data_array[data_ind] != NULL )
    {
      delete m_sum_data_array[data_ind];
      m_sum_data_array[data_ind] = NULL;
    }
    m_sum_data_array.clear();
  }
  for( data_ind = 0; data_ind < m_data_array.size(); data_ind++ )
  {
    if( m_data_array[data_ind] != NULL )
    {
      delete m_data_array[data_ind];
      m_data_array[data_ind] = NULL;
    }
    m_data_array.clear();
  }

}

} // namespaceS
