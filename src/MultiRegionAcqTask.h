// ============================================================================
//
// = CONTEXT
//		TANGO Project - GroupManager
//
// = File
//		GroupManagerTask.h
//
// = AUTHOR
//    N.Leclercq - SOLEIL & JC.Pret - APSIDE
//
// ============================================================================

#ifndef _MULTI_REGION_ACQ_TASK_H_
#define _MULTI_REGION_ACQ_TASK_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <yat/any/GenericContainer.h>
#include <yat/memory/DataBuffer.h>
#include <yat4tango/DeviceTask.h>
#include <yat4tango/DynamicAttributeManager.h>

#define DEFAULT_CONFIG_STR "undefined"

namespace ScientaMultiRegion_ns
{

typedef struct RegionConfig
{
  Tango::DevString mode;
  Tango::DevDouble low_energy;
  Tango::DevDouble high_energy;
  Tango::DevDouble fix_energy;
  Tango::DevDouble energy_step;
  Tango::DevDouble step_time;
  Tango::DevString lens_mode;
  Tango::DevDouble pass_energy;

  RegionConfig ()
  : mode (DEFAULT_CONFIG_STR), 
    low_energy(0.),
    high_energy(0.),
    fix_energy(0.),
    energy_step(0.),
    step_time(0.),
    lens_mode (DEFAULT_CONFIG_STR), 
    pass_energy(0.)
  {
  }

  RegionConfig (const RegionConfig & src)
  {
    *this = src;
  }

  const RegionConfig & operator= (const RegionConfig & src)
  {
    mode = src.mode;
    low_energy = src.low_energy;
    high_energy = src.high_energy;
    fix_energy = src.fix_energy;
    energy_step = src.energy_step;
    step_time = src.step_time;
    lens_mode = src.lens_mode;
    pass_energy = src.pass_energy;
	return *this;
  }

} RegionConfig;

typedef struct AcquisitionConfig
{
  Tango::DevULong num_regions;
  Tango::DevDouble excitation_energy;
  Tango::DevString pass_mode;
  Tango::DevShort detector_first_x_channel;
  Tango::DevShort detector_last_x_channel;
  Tango::DevShort detector_first_y_channel;
  Tango::DevShort detector_last_y_channel;
  Tango::DevShort detector_slices;
  Tango::DevLong channels;
  Tango::DevShort ADC_mode;
  bool has_ADC_mask_support;
  Tango::DevShort ADC_mask;
  bool has_ADC_discriminator_support;
  Tango::DevShort discriminator_level;

  std::vector<RegionConfig> region_attrs;

  bool initialised;

  AcquisitionConfig ()
    : num_regions(0),
      excitation_energy(0.),
      pass_mode(DEFAULT_CONFIG_STR),
      detector_first_x_channel(0),
      detector_last_x_channel(0),
      detector_first_y_channel(0),
      detector_last_y_channel(0),
      detector_slices(0),
      channels(0),
      ADC_mode(0),
      has_ADC_mask_support(false),
      ADC_mask(0),
      has_ADC_discriminator_support(false),
      discriminator_level(0),
      region_attrs(),
      initialised(false)
   {
   }


  AcquisitionConfig (const AcquisitionConfig& src)
   {
      *this = src;
   }

  const AcquisitionConfig& operator= (const AcquisitionConfig& src)
   {
      num_regions = src.num_regions;
      excitation_energy = src.excitation_energy;
      pass_mode = src.pass_mode;
      detector_first_x_channel = src.detector_first_x_channel;
      detector_last_x_channel = src.detector_last_x_channel;
      detector_first_y_channel = src.detector_first_y_channel;
      detector_last_y_channel = src.detector_last_y_channel;
      detector_slices = src.detector_slices ;
      channels = src.channels;
      ADC_mode = src.ADC_mode;
      has_ADC_mask_support = src.has_ADC_mask_support;
      ADC_mask = src.ADC_mask;
      has_ADC_discriminator_support = src.has_ADC_discriminator_support;
      discriminator_level = src.discriminator_level;
      region_attrs = src.region_attrs;
      initialised = src.initialised;
   }

} AcquisitionConfig;

// ============================================================================
// class: MultiRegionAcqTask
// ============================================================================
class MultiRegionAcqTask : public yat4tango::DeviceTask
{
public:

  MultiRegionAcqTask(Tango::DeviceImpl * host_device);
  MultiRegionAcqTask(Tango::DeviceImpl * host_device, long max_reg_num, const std::string &target_dev );

  virtual ~MultiRegionAcqTask();

	//- specialization of yat4tango::DeviceTask::go
  //---------------------------------------------------------------------------------------
  //- TANGO THREADING CONSTRAINT: THIS METHOD IS EXPECTED TO BE CALLED FROM <INIT_DEVICE>
  //- OR (AT LEAST) FROM A TANGO DEVICE THREAD (SEE THE TANGO DEVICE MONITOR FOR DETAILS)
  //---------------------------------------------------------------------------------------
  virtual void go (size_t tmo_ms = kDEFAULT_MSG_TMO_MSECS)
    throw (Tango::DevFailed);

	//- specialization of yat4tango::DeviceTask::exit
  //---------------------------------------------------------------------------------------
  //- TANGO THREADING CONSTRAINT: THIS METHOD IS EXPECTED TO BE CALLED FROM <INIT_DEVICE>
  //- OR (AT LEAST) FROM A TANGO DEVICE THREAD (SEE THE TANGO DEVICE MONITOR FOR DETAILS)
  //---------------------------------------------------------------------------------------
  virtual void exit ()
    throw (Tango::DevFailed);

  //- starts task's periodic activity (a <tmo_ms> of 0 means asynchronous exec)
  void start_periodic_activity (size_t tmo_ms = 0)
    throw (Tango::DevFailed);

  //- stops task's periodic activity (a <tmo_ms> of 0 means asynchronous exec)
  void stop_periodic_activity (size_t tmo_ms = 0)
    throw (Tango::DevFailed);


  //- encapsulates the periodic activity
  void periodic_job()
    throw (Tango::DevFailed);

  // Attribute accessors

  void spectrum_rd_cb (yat4tango::DynamicAttributeReadCallbackData& d)
    throw (Tango::DevFailed);

  void image_rd_cb (yat4tango::DynamicAttributeReadCallbackData& d)
    throw (Tango::DevFailed);

  void set_region_num( Tango::DevLong reg_num );
  Tango::DevLong get_region_num();

  Tango::DevULong get_region_in_progress();

  void set_detector_slices( Tango::DevShort det_sli );
  Tango::DevShort get_detector_slices();

  void set_excitation_energy( Tango::DevDouble exc_enr );
  Tango::DevDouble get_excitation_energy();

  void set_pass_mode( Tango::DevString pass_mode );
  Tango::DevString get_pass_mode();

  void set_detector_first_x_channel( Tango::DevShort first_x_chan );
  Tango::DevShort get_detector_first_x_channel();

  void set_detector_last_x_channel( Tango::DevShort last_x_chan );
  Tango::DevShort get_detector_last_x_channel();

  void set_detector_first_y_channel( Tango::DevShort first_y_chan );
  Tango::DevShort get_detector_first_y_channel();

  void set_detector_last_y_channel( Tango::DevShort last_y_chan );
  Tango::DevShort get_detector_last_y_channel();

  void set_ADC_mode( Tango::DevShort adc_mode );
  Tango::DevShort get_ADC_mode();

  void set_ADC_mask( Tango::DevShort adc_mask );
  Tango::DevShort get_ADC_mask();

  void set_discriminator_level( Tango::DevShort disc_lvl );
  Tango::DevShort get_discriminator_level();

  Tango::DevVarDoubleArray get_scalar_attr( long attr_ind, const std::string &attr_name );

  void reset_configuration();

  void validate_configuration();

  void init_region_configuration();

  void start_cmd();

  void stop_cmd();

  void validate_cfg_cmd();

  void reset_cfg_cmd();

  void start_acquisition()
    throw (Tango::DevFailed);

  void stop_acquisition()
    throw (Tango::DevFailed);

  void write_common_attrs( vector<Tango::DeviceAttribute> &attr_vect );

  void write_specific_attrs( int region_ind, vector<Tango::DeviceAttribute> &attr_vect );

  Tango::DevVarDoubleArray& read_energy_step();
  void write_energy_step( const Tango::DevDouble *double_data, int len );

  Tango::DevVarDoubleArray& read_high_energy();
  void write_high_energy( const Tango::DevDouble *long_data, int len );

  Tango::DevVarDoubleArray& read_low_energy();
  void write_low_energy( const Tango::DevDouble *long_data, int len );

  Tango::DevVarDoubleArray& read_fix_energy();
  void write_fix_energy( const Tango::DevDouble *long_data, int len );

  Tango::DevVarDoubleArray& read_step_time();
  void write_step_time( const Tango::DevDouble *long_data, int len );

  Tango::DevVarStringArray& read_lens_mode();
  void write_lens_mode( const Tango::ConstDevString *long_data, int len );

  Tango::DevVarStringArray& read_mode();
  void write_mode( const Tango::ConstDevString *str_data, int len );

  Tango::DevVarDoubleArray& read_pass_energy();
  void write_pass_energy( const Tango::DevDouble *double_data, int len );

  Tango::DevState get_state();
  std::string get_status();

  void reset_data_vect();

protected:
	//- process_message (implements yat4tango::DeviceTask pure virtual method)
	virtual void process_message (yat::Message& msg)
    throw (Tango::DevFailed);

  Tango::DeviceProxy *m_target_proxy;

  Tango::DeviceImpl *m_host_device;

  //- a mutex to protect the data against "race condition"
  yat::Mutex m_data_lock; 

  long m_max_region_num;

  bool m_data_has_changed;

  Tango::DevULong m_region_in_progress;

  Tango::DevLong m_slices;
  Tango::DevLong m_channels;

  //- the dynamic attribute manager
  yat4tango::DynamicAttributeManager m_dam;

  AcquisitionConfig m_config;

  Tango::DevVarStringArray m_mode_array;
  Tango::DevVarDoubleArray m_high_energy_array;
  Tango::DevVarDoubleArray m_low_energy_array;
  Tango::DevVarDoubleArray m_fix_energy_array;
  Tango::DevVarDoubleArray m_energy_step_array;
  Tango::DevVarDoubleArray m_step_time_array;
  Tango::DevVarStringArray m_lens_mode_array;
  Tango::DevVarDoubleArray m_pass_energy_array;

  //- device state
  Tango::DevState m_dev_state;
  //- device status
  std::string m_dev_status;
  
  std::vector<Tango::DevVarDoubleArray*> m_channel_scale_array;
  std::vector<Tango::DevVarDoubleArray*> m_slice_scale_array;
  std::vector<Tango::DevVarDoubleArray*> m_sum_data_array;
  std::vector<Tango::DevVarDoubleArray*> m_data_array;
  
};

} // namespace

#endif

